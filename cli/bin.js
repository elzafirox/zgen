#!/usr/bin/env node

'use strict';

const FONTS = require('cfonts'),
		//argv = require('yargs').argv,
		version = require('../package.json').version,
		command = require('./command').parse(),
		cli = require('./cli'),
		init = require('./command/init'),
		generate = require('./command/generate'),
		clear = require('clear');//,
		/*build = require(),
		publish = require();*/

// Borramos pantalla
clear();


new FONTS({
	'text': '= Zgen =',
	'font': 'block',
	'colors': ['yellow','blue'],
	'background': 'Black',
	'letterSpacing': 2,
	'space': true,
	'maxLength': '12'
});

new FONTS({
	'text': '	      Generador de aplicaciones angularjs '+version,
	'font': 'console',
	'colors': ['White'],
	'background': 'Black',
	'letterSpacing': 2,
	'space': true,
	'maxLength': '100'
});

if( command._[0] === 'init' ){
	init();
}else if( command._[0].split(':')[0] === 'generate' ){
	generate( command );
}
