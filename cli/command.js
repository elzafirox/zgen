module.exports = (() => {

	'use strict';

	const parser = require("nomnom").script("zgen");

	// Comando para iniciar el proyecto de documentacion y su archivo zdoc.json
	parser.command('init').help("Iniciar un nuevo proyecto");

	// Comando para generar una nueva ruta
	parser.command('generate:module').help('Genera un nuevo module en el proyecto');

	// Comando para generar una nueva ruta
	parser.command('generate:route').help('Genera una nueva ruta en un modulo');

	// Comando para generar un componente
	parser.command('generate:component').help('Genera un nuevo componente para el proyecto');

	// Comando para generar un controlador
	parser.command('generate:controller').help('Genera un nuevo controlador');

	// Comando para generar una nueva vista
	parser.command('generate:view').help('Genera un archivo de vista');

	// Comando para generar un filtro
	parser.command('generate:filter').help('Genera un nuevo filtro');

	// Comando para generar una directiva
	parser.command('generate:directive').help('Genera una nueva directiva');

	// Comando para generar un nuevo serivicio
	parser.command('generate:service').help('Genera un nuevo servicio');

	// Comando para generar un provider
	parser.command('generate:provider').help('Genera un nuevo provider');

	// Comando para generar una factory
	parser.command('generate:factory').help('Genera una nueva factory');

	// Comando para un decorador
	//parser.command('generate:decorator').help('Genera un nuevo decorador');

	// Regresa el numero de la version del generador
	parser.command('version')
		.callback(function(opts) {
			return 'zgen v'+require('../package.json').version;
		})
		.help("Retorna el numero de la version de zgen");

	return parser;

})();
