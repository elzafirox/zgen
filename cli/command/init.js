module.exports = (() => {
	'use strict';

	return ()=>{

		// Cargamos las librerias
		const path = require('path'),
				inquirer = require('inquirer'),
				camelCase = require('camelcase'),
				chalk = require('chalk'),
				_ = require('lodash'),
				fs = require('fs'),
				async = require('async'),
				fse = require('fs-extra'),
				dirProyect = path.resolve('./'),
				dirTemplate = path.join(__dirname, '..', '..', 'template'),
				fileDirProyect = [
					'app',
					'task',
					'.bowerrc',
					'.csslintrc',
					'.htmlhintrc',
					'.jshintrc',
					'.slugignore',
					'bower.json',
					'gulpfile.js',
					'package.json'
				];

		// fse.copy( path.join(__dirname, '..', '..', 'template', 'app'), path.join(dirProyect, 'app'), function (err) {
		// 	if (err) return console.error(err)
		// 	console.log('success!')
		// });

		inquirer.prompt([
			{
				type: 'input',
				name: 'name',
				message: 'Nombre del proyecto:',
				validate:  ( value )=> {
					if( value.length < 1 )
						return "Favor ingresar un nombre de proyecto valido";
					return true;
				},
				filter: ( value )=> {
					return camelCase(value);
				}
			},
			{
				type: 'input',
				name: 'version',
				message: 'Version del proyecto:',
				validate:  ( value )=> {
					if( !value.match( /^[0-9]{1,10}\.[0-9]{1,10}\.[0-9]{1,10}/i ) )
						return "Favor ingresar una version de proyecto valida";
					return true;
				}
			},
			{
				type: 'input',
				name: 'descripcion',
				message: 'Descripcion del proyecto:'
			}
		]).then( (resp)=>{
			// Compiamos los archivos de las plantillas
			async.each(fileDirProyect, (file, callback)=>{
				fse.copy( path.join(dirTemplate, file), path.join(dirProyect, file), (err)=> {
					if(err){
						console.log(err);
						callback(err);
					}else{
						callback();
					}
				});
			}, (err)=>{
				if(err){
					console.log('Ocurrio un error');
					return 0;
				}

				let plantilla = [
					{	file: 'package.json',
						param: [
							{ key: '<[%name%]>', value: resp.name },
							{ key: '<[%descripcion%]>', value: resp.descripcion  },
							{ key: '<[%version%]>', value: resp.version }
						]
					},
					{	file: 'bower.json',
						param: [
							{ key: '<[%name%]>', value: resp.name },
							{ key: '<[%descripcion%]>', value: resp.descripcion  },
							{ key: '<[%version%]>', value: resp.version }
						]
					},
					{	file: 'app/config.js',
						param: [
							{ key: '<[%name%]>', value: resp.name }
						]
					}
				];

				// Creamos el archivo gitinore
				fs.writeFileSync(
					path.join(dirProyect, '.gitignore'),
					fs.readFileSync(
						path.join(dirTemplate, 'gitignore'),{encoding: 'utf8'}
					),
					{ encoding: 'utf8' }
				);

				// Agregamos las variables
				async.each(plantilla, (operation, callback)=>{
					// optenemos el contenido del archivo
					let filContent = fs.readFileSync( path.join(dirProyect, operation.file),{encoding: 'utf8'} );
					//Cambiamos los valores
					for (var i = 0; i < operation.param.length; i++) {
						filContent = filContent.replace(operation.param[i].key, operation.param[i].value )
					}
					// Guardamos el resultado
					fs.writeFileSync( path.join(dirProyect, operation.file), filContent, { encoding: 'utf8' } );

					// Indicamos que termimanos
					callback();
				}, (error)=>{
					if( error ){
						console.log('Ocurrio un error');
					}else{
						// Pasamos a instalar las dependecias
						async.eachSeries(['bower','npm'], (cmd, callback)=>{
							var exec = exec = require('child_process').exec,
								x = exec(cmd + ' install', {cwd: dirProyect});

							x.stdout.on('data', function (data) {
								console.log(data);
							});

							x.stderr.on('data', function (data) {
								console.log(data);
							});

							x.on('exit', function (code) {
								if (code !== 0) throw new Error(item + ' failed');
								console.log('Dependencias instaladas \n');
								callback();
							});

						}, (error)=>{
							console.log('Proyecto generados');
						});

					}
				});

			});
		});
	};
})();
