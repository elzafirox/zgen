module.exports = (() => {

	'use strict';

	return (commandParam)=>{

		const	chalk = require('chalk'),
				jsonfile = require('jsonfile'),
				inquirer = require('inquirer'),
				camelCase = require('camelcase'),
				fs = require('fs'),
				fse = require('fs-extra'),
				del = require('del'),
				path = require('path'),
				os = require('os'),
				async = require('async'),
				dirProyect = path.resolve('./'),
				dirTemplate = path.join(__dirname, '..', '..', 'template'),
				listDir = (srcpath)=> {
					return fs.readdirSync(srcpath).filter(function(file) {
						return fs.statSync(path.join(srcpath, file)).isDirectory();
					});
				},
				modules = listDir( path.join(dirProyect,'app','modules') ),
				element = commandParam._[0].split(':')[1];

		if( element === 'module'  ){

			// Consultamos el nombre del elemento.
			inquirer.prompt([
				{
					type: 'input',
					name: 'name',
					message: 'Nombre del Modulo: ',
					validate:  ( value )=> {
						if( value.length < 1 )
							return "Favor ingresar un nombre valido";
						return true;
					},
					filter: ( value )=> {
						return camelCase(value);
					}
				}
			]).then( (resp)=>{

				var archivos = [
					{
						in: path.join( dirTemplate, 'modulo', 'template.client.module.js'),
						out: path.join( dirProyect, 'app', 'modules', resp.name, resp.name + '.client.module.js')
					},
					{
						in: path.join( dirTemplate, 'modulo', 'template.client.config.js'),
						out: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.config.js')
					},
					{
						in: path.join( dirTemplate, 'modulo', 'template.client.routes.js'),
						out: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.routes.js')
					},
					{
						in: path.join( dirTemplate, 'modulo', 'template.client.run.js'),
						out: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.run.js')
					}
				];

				// Recorremos los archivos que se van a copiar
				async.each(archivos, (file, callback)=>{
					fse.copy( file.in, file.out, (err)=> {
						if(err){
							console.log(err);
							callback(err);
						}else{
							callback();
						}
					});
				}, (err)=>{

					if (err){
						console.log(err);
						return 0;
					}


					// Cambiamos los valores
					let plantilla = [
						{
							file: path.join( dirProyect, 'app', 'modules', resp.name, resp.name + '.client.module.js'),
							param: [
								{ key: '<[%module%]>', value: resp.name }
							]
						},
						{
							file: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.config.js'),
							param: [
								{ key: '<[%module%]>', value: resp.name }
							]
						},
						{
							file: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.routes.js'),
							param: [
								{ key: '<[%module%]>', value: resp.name }
							]
						},
						{
							file: path.join( dirProyect, 'app', 'modules', resp.name, 'config', resp.name + '.client.run.js'),
							param: [
								{ key: '<[%module%]>', value: resp.name }
							]
						}
					];

					// Cambiar regex
					async.each(plantilla, (operation, callback)=>{
						// optenemos el contenido del archivo
						let filContent = fs.readFileSync( operation.file, {encoding: 'utf8'} );
						//Cambiamos los valores
						for (var i = 0; i < operation.param.length; i++) {
							filContent = filContent.replace(operation.param[i].key, operation.param[i].value )
						}
						// Guardamos el resultado
						fs.writeFileSync( operation.file, filContent, { encoding: 'utf8' } );

						// Indicamos que termimanos
						callback();
					}, (error)=>{

						// Creamos los directorios
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'controllers') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'css') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'directives') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'filters') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'img') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'services') );
						fse.emptyDirSync( path.join( dirProyect, 'app', 'modules', resp.name, 'views') );

						// Termino el proceso
						console.log('Modulo creado');
					});

				});

			});

		}else{
			// Preguntamos en que modulo se instalara
			inquirer.prompt([
				{
					type: 'list',
					name: 'name',
					message: 'Modulo: ',
					choices: modules
				}
			]).then(function(module){

				var file = '',
					fileName = '',
					plantilla = [];

				// Consultamos el nombre del elemento.
				inquirer.prompt([
					{
						type: 'input',
						name: 'name',
						message: 'Nombre del ' + element + ': ',
						validate:  ( value )=> {
							if( value.length < 1 )
								return "Favor ingresar un nombre valido";
							return true;
						},
						filter: ( value )=> {
							return camelCase(value);
						}
					}
				]).then( (resp)=>{
					// comprovamos que es lo que se quiere crear.
					if( element === 'route' ){

					}else if( element === 'component' ){
						file = path.join( dirTemplate, 'element', 'template.client.component.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'components',
							resp.name + '.client.component.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'controller' ){
						file = path.join( dirTemplate, 'element', 'template.client.controller.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'controllers',
							resp.name + '.client.controller.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'view' ){
						file = path.join( dirTemplate, 'element', 'template.client.view.html');
						fileName = path.join( dirProyect, 'app','modules', module.name, 'views', resp.name + '.client.view.html');
					}else if( element === 'filter' ){
						file = path.join( dirTemplate, 'element', 'template.client.filter.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'filters',
							resp.name + '.client.filter.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'directive' ){
						file = path.join( dirTemplate, 'element', 'template.client.directive.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'directives',
							resp.name + '.client.directive.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'service' ){
						file = path.join( dirTemplate, 'element', 'template.client.service.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'services',
							resp.name + '.client.service.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'provider' ){
						file = path.join( dirTemplate, 'element', 'template.client.provider.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'providers',
							resp.name + '.client.provider.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'factory' ){
						file = path.join( dirTemplate, 'element', 'template.client.factory.js');
						fileName = path.join(
							dirProyect,
							'app',
							'modules',
							module.name,
							'factorys',
							resp.name + '.client.factory.js'
						);
						plantilla = [
							{
								file: fileName,
								param: [
									{ key: '<[%module%]>', value: module.name },
									{ key: '<[%name%]>', value: resp.name  }
								]
							}
						];
					}else if( element === 'decorator' ){
						console.log('Aun no disponible');
					}else{
						console.log('No es posible crear el ' + element);
						return 0;
					}

					// Copiamos la plantilla
					fse.copy( file, fileName, (err)=> {
						if(err){
							console.log(err);
							return 0;
						}else{
							async.each(plantilla, (operation, callback)=>{
								// optenemos el contenido del archivo
								let filContent = fs.readFileSync( operation.file, {encoding: 'utf8'} );
								//Cambiamos los valores
								for (var i = 0; i < operation.param.length; i++) {
									filContent = filContent.replace(operation.param[i].key, operation.param[i].value )
								}
								// Guardamos el resultado
								fs.writeFileSync( operation.file, filContent, { encoding: 'utf8' } );

								// Indicamos que termimanos
								callback();
							}, (error)=>{
								// Termino el proceso
								console.log('Elemento creado con exito');
							});
						}
					});

				});

			});
		}

	};

})();
