'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	htmlhint = require('gulp-htmlhint'),
	dom  = require('gulp-dom'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	ngAnnotate = require('gulp-ng-annotate'),
	templateCache = require('gulp-angular-templatecache');

/**
* =================================================
*	Tarea encargada de revisar el codigo html
* =================================================
*/
gulp.task('rev-html', function(){
	return  gulp.src( conf.appFiles.html )
			.pipe( htmlhint( '.htmlhintrc' ) )
			.pipe( htmlhint.reporter("htmlhint-stylish") )
			.pipe( htmlhint.failReporter({ suppress: true }) );
});

/**
* =================================================
*  	Tarea encargada de comprimir el codigo html
* =================================================
*/
gulp.task('html:dist', function(){
	return gulp.src( conf.appFiles.html )
		.pipe( htmlmin({
					collapseWhitespace: true,
					keepClosingSlash: true,
					removeComments: true,
					removeEmptyAttributes: true,
					removeRedundantAttributes: true
				}
			)
		)
		.pipe( templateCache({
			module: 'viewApp'
		}) )
		.pipe( ngAnnotate() )
		.pipe( rename({suffix: '.min'}) )
		.pipe( gulp.dest( conf.paths.dist ) );
});

/**
* =================================================
*  	Tarea encargada de comprimir el codigo html
* =================================================
*/
gulp.task('html:remove:livereload', function() {
	return gulp.src( conf.paths.dist + 'index.html' )
		.pipe(dom(function(){
			var child = this.getElementById('livereload');
			child.parentNode.removeChild(child);
			return this;
		}))
		.pipe( gulp.dest( conf.paths.dist ) );
});