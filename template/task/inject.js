'use strict';

var gulp = require('gulp'),
	conf = require('./conf'),
	inject = require('gulp-inject'),
	rename = require('gulp-rename'),
	mainBowerFiles = require('main-bower-files');


/**
* ========================================================
* 	  Tarea para inyectar dependencias para produccion
* ========================================================
*/

gulp.task('inject:production', function(){
	return gulp.src('./app/_template_/index.template.view.html')
		// Inyectamos los archivos de bower		
		.pipe(
			inject(
				gulp.src(
					mainBowerFiles({
						paths: conf.bower
						}
					),
					{read: false}
				),
				{name: 'bower'}
			)
		)

		// Inyectamos los archivos de angularjs
		.pipe(
			inject(
				gulp.src(['./app/cnfg.js', './application.min.js', './application.min.css']),
				{name: 'app'}
			)
		)

		//Cambiamos el nombre asignado
		.pipe( rename('index.html') )

		// Ejecutamos el resultado
		.pipe( gulp.dest( conf.paths.src ) );
});

/**
* ==========================================
* 	  Tarea para inyectar dependencias
* ==========================================
*/

gulp.task('inject:developer', function(done){
	return gulp.src('./app/_template_/index.template.view.html')
	// Inyectamos los archivos de bower		
	.pipe(
		inject(
			gulp.src(
				mainBowerFiles({
						paths: conf.bower
					}
				),
				{
					read: false
				}
			),
			{
				name: 'bower'
			}
		)
	)

	// Inyectamos los archivos de angularjs
	.pipe(
		inject(
			gulp.src( conf.appFiles.assets.css.concat( conf.appFiles.css.concat( conf.appFiles.js ) ) ),
			{
				name: 'app'
			}
		)
	)

	//Cambiamos el nombre asignado
	.pipe( rename('index.html') )

	// Ejecutamos el resultado
	.pipe( gulp.dest( conf.paths.src ) );
});