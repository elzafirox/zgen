'use strict';

var gulp = require('gulp'),
	conf = require('./conf'),
	clear = require('clear'),
	livereload = require('gulp-livereload'),
	plumber = require('gulp-plumber'),
	watch = require('gulp-watch'),
	reloadBrowser = function(files){
		clear();
		livereload.changed(files);
	};

// Tarea encargada de vigilar los cambios en los archivos, para reiniciar el Browser , ['inject']
gulp.task('watch', function(){
	
	/**
	* ==========================================
	* 		Revisamos los archivos de HTML
	* ==========================================
	*/

	watch( conf.appFiles.html, reloadBrowser)
		.on('add', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		})
		.on('change', function(){
			// Ejecutamos la revision del codigo html
			gulp.start('rev-html');
		})
		.on('unlink', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		});

	/**
	* ==========================================
	* 		Revisamos los archivos de JS
	* ==========================================
	*/

	watch( conf.appFiles.js, reloadBrowser)
		.on('add', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		})
		.on('change', function(){
			// Ejecutamos la revision del codigo js
			gulp.start('rev-js');
		})
		.on('unlink', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		});

	/**
	* ==========================================
	* 		Revisamos los archivos de CSS
	* ==========================================
	*/

	watch( conf.appFiles.css, reloadBrowser)
		.on('add', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		})
		.on('change', function(){})
		.on('unlink', function(){
			// Inyectamos las dependencias
			gulp.start('inject:developer');
		});

	/**
	* ==========================================
	* 		Revisamos los archivos de conf
	* ==========================================
	*/

	watch( ['app/config.app.json','app/global.app.json'], reloadBrowser)
		.on('add', function(){
			gulp.start('config:ng','inject:developer');
		})
		.on('change', function(){
			gulp.start('config:ng','inject:developer');
		})
		.on('unlink', function(){
			gulp.start('config:ng','inject:developer');
		});

});