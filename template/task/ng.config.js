'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	rename = require('gulp-rename'),
	environments = require('gulp-environments'),
	gulpNgConfig = require('gulp-ng-config'),
	production = environments.production;

/**
* ================================================================
*	 Tarea para generar el archivo de configuracion angularjs
* ================================================================
*/
gulp.task('config:ng', function(){
	return gulp.src( conf.appConfig.conf )
				.pipe( gulpNgConfig('app.Config',{
					environment: production() ? 'production' : 'local',
					constants: require( conf.appConfig.global ),
					pretty: true,
					wrap: '/* jshint ignore:start */\n <%= module %> \n /* jshint ignore:end */'
				}) )
				.pipe( rename({ basename: 'app.config' }) )
				.pipe( gulp.dest( conf.paths.app ) );
});
