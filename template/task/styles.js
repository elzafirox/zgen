'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	uglifycss = require('gulp-uglifycss'),
	concat = require('gulp-concat'),
	replace = require('gulp-replace'),
	ngAnnotate = require('gulp-ng-annotate');

/**
* =====================================================
*  	  Tarea encargada de comprimir el codigo css
* =====================================================
*/

gulp.task('dist-css', function(){
	return gulp.src( conf.appFiles.assets.css.concat( conf.appFiles.css ) )
			.pipe( concat('application.css', {newLine: ';'}) )
			.pipe( replace(/\.\.\/font\//g, '/app/assets/fonts/' ) )
			//.pipe(autoprefixer())
			.pipe( uglifycss() )
			.pipe( rename({suffix: '.min'}) )
			// Agregamos la ruta correcta en el servicio
			.pipe( replace(/\/app\//g, './app/' ) )
			.pipe( gulp.dest( conf.paths.src ) );
});