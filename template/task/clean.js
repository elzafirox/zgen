'use strict';

var gulp = require('gulp'),
	del = require('del'),
	conf = require('./conf');

/**
* ==========================================
* 	  Tarea para borrar directorios
* ==========================================
*/

gulp.task('clean', function(done){
	return del([ conf.paths.src + 'index.html', conf.paths.tmp ]);
});

/**
* ==========================================
* 	  Tarea para borrar dist
* ==========================================
*/
gulp.task('clean:dist', function(done){
	return del( conf.paths.dist );
});

/**
* ==========================================
* 	  Tarea para borrar dist gulp
* ==========================================
*/
gulp.task('clean:gulp', function(done){
	return del( './distGulp' );
});

/**
* ==============================================
* 	  Tarea para borrar el html de las vistas
* ==============================================
*/
gulp.task('clean:html', function(done){
	return del( conf.paths.dist + 'app/modules' );
});

/**
* ==========================================
* 	  Tarea para borrar la app dist
* ==========================================
*/

gulp.task('clean:dist:app', function(done){
	return del(['./application.min.js','./application.min.css']);
});