'use strict';

var path = require('path'),
	gulp = require('gulp'),
	conf = require('./conf'),
	clear = require('clear'),
	livereload = require('gulp-livereload'),
	gutil = require('gulp-util'),
    http = require('http'),
    st = require('st');

// Tarea encargada de levantar un servidor a modo de desarollo
gulp.task('serve', ['watch'], function(done){
	// Iniciamos el servidor
	return http.createServer(
		st({ path: conf.paths.src, index: conf.server.index, cache: conf.server.cache })
	).listen( conf.server.port  , function(){
		// Indicamos el inicio del servidor
		gutil.log( gutil.colors.green( 'Servidor iniciado en el puerto:' + conf.server.port ) );
		// Cargamos livereload
		livereload.listen({ host: 'localhost' });
		done();
	});
});