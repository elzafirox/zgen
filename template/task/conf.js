'use strict';

var gutil = require('gulp-util');

// Archivos de la configuracion
exports.appConfig = {
	global : '../app/global.app.json',
	conf: 'app/config.app.json'
};

// Variables de configuracion de directorios
exports.paths = {
	src : './',
	dist: './dist/',
	tmp : './.tmp/',
	app: './app/'
};

// Configuracion referente al servidor
exports.server= {
	index: 'index.html',
	cache: false,
	port: 8000
};

// Configuraciones para la inyeccion de dependencias de bower
exports.bower = {
	bowerDirectory: './app/lib',
	bowerrc: '.bowerrc',
	bowerJson: './bower.json'
};

// Archivos de la aplicacion, estos son tos los requeridos para las vistas
exports.appFiles = {
	js:[
		'app/app.config.js',
		'app/config.js',
		'app/application.js',
		'app/modules/core/*.client.module.js',
		'app/modules/core/config/*.js',
		'app/modules/core/filters/*.js',
		'app/modules/core/services/*.js',
		'app/modules/core/directives/*.js',
		'app/modules/core/controllers/*.js',
		'app/modules/*/*.client.module.js',
		'app/modules/**/js/*.js',
		'app/modules/*[!core]*/config/*.js',
		'app/modules/*[!core]*/filters/*.js',
		'app/modules/*[!core]*/services/*.js',
		'app/modules/*[!core]*/directives/*.js',
		'app/modules/*[!core]*/controllers/*.js'
	],
	css:[
		'app/modules/**/css/*.css'
	],
	html: [
		'app/modules/**/views/**/*.html'
	],
	index: [
		'index.html'
	],
	assets: {
		css: [
			'app/assets/css/styles.css',
			'app/assets/css/plugins.css',
			'app/assets/css/themes/theme-1.css',
			'app/assets/css/themes/lyt4-theme-1.css'
		]
	}
};


// Inyeccion de dependencias
exports.wiredep = {};

// Handler para error de los plugins
exports.errorHandler = function(title){
    'use strict';

	return function(err){
		gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
		//this.emit('end');
	};
};