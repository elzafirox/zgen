'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	replace = require('gulp-replace'),
	ngAnnotate = require('gulp-ng-annotate'),
	jshint = require('gulp-jshint'),
	jshintStylish = require('jshint-stylish');

/**
* ============================================
*	Tarea encargada de revisar el codigo js
* ============================================
*/
gulp.task('rev-js', function(){
	return  gulp.src( conf.appFiles.js )
			.pipe( jshint() )
			.pipe( jshint.reporter(jshintStylish) );
});

/**
* ============================================
*  Tarea encargada de comprimir el codigo js
* ============================================
*/
gulp.task('dist-js', function(){
	
	var arrayFileJs = conf.appFiles.js;

	// Eliminando primer elemento
	delete arrayFileJs[0];

	return gulp.src( arrayFileJs )
			.pipe( concat('application.js', {newLine: ';'}) )
			.pipe( ngAnnotate() )
			.pipe( rename({suffix: '.min'}) )
			.pipe( uglify({mangle: true}) )
			// Agregamos la ruta correcta en el servicio
			.pipe( replace(/\/app\//g, './app/' ) )
			.pipe( gulp.dest( conf.paths.src ) );
});