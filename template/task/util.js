'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	replace = require('gulp-replace'),
	htmlmin = require('gulp-htmlmin');

/**
* ======================================================
*	Tarea para agregar ruta absoluta en los archivos
* ======================================================
*/

gulp.task('absolute-path-compress', function(){
	return gulp.src( conf.paths.dist + 'index.html' )
			.pipe( replace( /=\"\/application\.min/g, '="./application.min' ) )
			.pipe( replace(/\/app\//g, './app/' ) )
			.pipe( htmlmin({
						collapseWhitespace: true,
						keepClosingSlash: true,
						removeComments: true,
						removeEmptyAttributes: true,
						removeRedundantAttributes: true
					}
				)
			)
			.pipe( gulp.dest( conf.paths.dist ) );
});