'use strict'

var gulp = require('gulp'),
	clear = require('clear'),
	plumber = require('gulp-plumber'),
	uglify = require('gulp-uglify'),
	runSequence = require('run-sequence');

/**
* ============================================
*	Tarea para generar el dist de gulp
* ============================================
*/
gulp.task('gulp-dist', function(){
	clear();

	runSequence(
		'clean:gulp',
		'gulp-task',
		'gulp-file'
	);
});

/**
* ============================================
*	Tarea para comprirmir el archivo gulp
* ============================================
*/
gulp.task('gulp-file', function(){
	return  gulp.src( './gulpfile.js' )
			.pipe( plumber() )
			.pipe( uglify() )
			.pipe( gulp.dest( './distGulp' ) );
});

/**
* ===================================================
*	 Tarea para comprirmir las tareas de gulp
* ===================================================
*/
gulp.task('gulp-task', function(){
	return  gulp.src( './task/*.js' )
			.pipe( plumber() )
			.pipe( uglify() )
			.pipe( gulp.dest( './distGulp/task/' ) );
});