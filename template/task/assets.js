'use strict'

var gulp = require('gulp'),
	conf = require('./conf'),
	uglify = require('gulp-uglify'),
	fileAssets = require('gulp-file-assets');

/**
* ================================================================
*			Tarea para optener los assets de un archivo
* ================================================================
*/
gulp.task('assets', function(){
	return gulp.src( conf.paths.src + 'index.html')
				.pipe( fileAssets() )
				.pipe( gulp.dest( conf.paths.dist ) );
});

/**
* ===============================================================
*		Tarea para clonar el directorio de fonts del tema.
* ===============================================================
*/
gulp.task('copy-fonts-awesome', function(){
	return gulp.src('./app/lib/font-awesome/fonts/fontawesome-webfont.woff2')
			.pipe( gulp.dest( conf.paths.dist + 'app/lib/font-awesome/fonts/') );
});

/**
* ================================================================
*		Tarea creada solo para el adan, dado que esta 
*		usando por alguna razon desconocida y extraña 
*		los iconos de bootstrap.
* ================================================================
*/
gulp.task('copy-fonts-gly', function(){
	return gulp.src('./app/lib/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2')
			.pipe( gulp.dest( conf.paths.dist + 'app/lib/bootstrap/dist/fonts/') );
});

/**
* ================================================================
*		Tarea para clonar el directorio de fonts del tema.
* ================================================================
*/
gulp.task('copy-fonts', function(){
	return gulp.src('./app/modules/core/font/**/*')
			.pipe( gulp.dest( conf.paths.dist + 'app/assets/fonts' ) );
});