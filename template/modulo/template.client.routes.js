'use strict';

angular.module('<[%module%]>').config([
	'$stateProvider', '$urlRouterProvider',
	function( $stateProvider, $urlRouterProvider ) {

		/**
		* ========================================================
		*	NOTA:
		*		Si la vista, usara el layout de la aplicacion
		*		el estado debe ser app.[NOMBRE], si no lo usara
		*		no debe contener el pre estado app.
		* ========================================================
		*/

		// Routes
		$stateProvider
			// .state('app.home', {
			// 	url:'/',
			// 	title: 'Home',
			// 	views: {
			// 		'content@app': {
			// 			controller:'HomeController',
			// 			controllerAs:'ctHome', //IMPORTANTE, para evitar errores siempre crear un alias
			// 			templateUrl: '/app/modules/core/views/home.client.view.html'
			// 		}
			// 	},
			// 	ncyBreadcrumb: {
			// 		label: 'Dashboard'
			// 	}
			// })
		;

	}
]);
