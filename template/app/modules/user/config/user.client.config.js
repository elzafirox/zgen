'use strict';

angular.module('user').config([
	'$httpProvider', '$urlRouterProvider',
	function( $httpProvider, $urlRouterProvider ) {
		$httpProvider.interceptors.push('RequestsInterceptor');
		$urlRouterProvider.deferIntercept();
	}
]);
