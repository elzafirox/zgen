'use strict';

/* jshint ignore:start */
angular.module('user').run([
	'$rootScope',
	'$state',
	'app',
	'$location',
	'Authentication',
	'localStorageService',
	'PermRoleStore',
	'Roles',
	'PermPermissionStore',
	'Permisos',
	'$urlRouter',
	'$uibModal',
	'ngAudio',
	function(
		$rootScope, 
		$state, 
		app, 
		$location, 
		Authentication, 
		localStorage, 
		PermRoleStore, 
		Roles, 
		PermissionStore,
		Permisos,
		$urlRouter,
		$uibModal,
		ngAudio
	) {

		var parseBool = function( v ){
				return v === '1';
			},
			capitalizeFirstLetter = function(string) {
				return string.charAt(0).toUpperCase() + string.slice(1);
			},
			error = ngAudio.load('app/assets/mp3/error.mp3');

		// Indicamos todo el volumen
		error.volume = 1;
		
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
			if( Authentication.isAuth() === true ){
				if( toState.name === 'login' ){
					event.preventDefault();
					$state.go('app.home');
				}else if( toState.name === 'lockscreen' ){
					if( !localStorage.get('lock') ){
						event.preventDefault();
						$state.go('app.home');
					}
				}else if( localStorage.get('lock') ){
					event.preventDefault();
					$state.go('lockscreen');
				}
			}else if( toState.name !== 'login' ){
				event.preventDefault();
				Authentication.logout();
				$state.go('login');
			}
		});

		$rootScope.$on('$stateChangeSuccess', function(event, toState){
		  event.preventDefault();
		});

		/**
		* -------------------------------------------------------------------
		*					Definimos los roles de la aplicacion
		* -------------------------------------------------------------------
		*/
		$urlRouter.sync();
		// Cargamos los roles
		Roles.query().$promise.then(function(resp){
			// Recorremos todos los roles
			for(var i = 0; i < resp.data.length; i++){
				var rol = resp.data[i];
				// Creamos el rol.
				PermRoleStore.defineRole(rol.name, function(){
					return rol.id === Authentication.user().rolid;
				});
			}

			/**
			* -------------------------------------------------------------------
			*					Definimos los permisos de la aplicacion
			* -------------------------------------------------------------------
			*/

			// Creamos un permiso para usuarios logueado
			PermissionStore.definePermission( 'AuthenticationUser', function(){
				return Authentication.isAuth();
			});

			PermissionStore.definePermission( 'LockUser', function(){
				if( !Authentication.isAuth() )
					return false;
				return localStorage.get('lock') || false;
			});

			Permisos.query({
				'filter':{
					'where':{
						'$eq': { 'userid': Authentication.user().id }
					}
				}
			}).$promise.then(function( resp ){
				for(var i = 0; i < resp.data.length; i++){
					var permiso = resp.data[i],
						rawName = permiso.state.state.split('.'),
						name = capitalizeFirstLetter( rawName[ ( rawName.length - 1)] );
					// Construimos los permisos necesarios
					PermissionStore.definePermission( 'delete' + name, function(){
						return parseBool(permiso.delete);
					});
					PermissionStore.definePermission( 'write' + name, function(){
						return parseBool(permiso.write);
					});
					PermissionStore.definePermission( 'read' + name, function(){
						return parseBool(permiso.read);
					});
					PermissionStore.definePermission( 'update' + name, function(){
						return parseBool(permiso.update);
					});
				}
				$urlRouter.listen();
			}).catch(function(){ $urlRouter.listen(); }); // En caso de que ocurra un error.
		}).catch(function(){ $urlRouter.listen(); }); // En caso de que ocurra un error.

		// Quedamos a la escucha del evento de que una ruta esta denegada
		$rootScope.$on('$routeChangePermissionDenied', function(event, nextRoute) {
			
			var insm = $uibModal.open({
				templateUrl: 'app/modules/user/views/denegadi.client.view.html',
				controller: 'denegadoController',
				size: 'sm'
			});

			// Reproducimos el audio una vez se renderea la pagina.
			insm.rendered.then(function(){
				error.play();
			});

			// Detenemos el audio
			insm.result.then(function(){}, function(){error.stop();});

		});
	}
]);
/* jshint ignore:end */