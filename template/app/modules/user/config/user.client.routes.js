'use strict';

angular.module('user').config([
	'$stateProvider', '$urlRouterProvider',
	function( $stateProvider, $urlRouterProvider ) {

		// Routes
		$stateProvider
			.state('login', {
				url: '/login',
				include: false,
				views:{
					root:{
						controller:'authenticationController',
						controllerAs:'vmLogin',
						templateUrl: 'app/modules/user/views/login.client.view.html'
					}
				}
			})
			.state('app.usuario', {
				url:'/usuario/:id/perfil',
				title: 'Perfil de usuario',
				include: false,
				views: {
					'content@app': {
						controller:'profileUserController',
						controllerAs:'vmProfile',
						templateUrl: '/app/modules/user/views/profile.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'perfil'
				}
			})
			.state('lockscreen', {
				url:'/usuario/lockscreen',
				title: 'Lock Screen',
				include: false,
				views: {
					root: {
						controller:'blockScreenController',
						controllerAs:'vmBScreen',
						templateUrl: '/app/modules/user/views/blockScreen.client.view.html'
					}
				}
			});
	}
]);
