'use strict';

angular.module('user').controller('blockScreenController', [
	'$scope', 'Usuarios', 'Authentication', '$state', 
	function($scope, Usuarios, Authentication, $state) {
		// Codigo del controlador
		var vm = this;

		// Datos del usuario
		$scope.usuario = Authentication.user();

		// Password
		$scope.password = '';

		// Marca de errro
		$scope.error = false;

		// Metodo para desbloquear
		$scope.unlock = function(){
			$scope.error = false;
			Authentication.unlock({
				username: $scope.usuario.username,
				password: $scope.password
			}).then(function(){
				$state.go('app.home');
			}, function(){
				$scope.password = '';
				$scope.error = true;
			});
		};

	}
]);
