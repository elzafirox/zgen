'use strict';

angular.module('user').controller('denegadoController', [
	'$scope', '$uibModalInstance',
	function($scope, $uibModalInstance) {
		// Codigo del controlador
		var vm = this;
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
]);
