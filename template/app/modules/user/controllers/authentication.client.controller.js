'use strict';

angular.module('user').controller('authenticationController', [
	'$scope','$state', 'Authentication',
	function($scope, $state, Authentication) {
		// Codigo del controlador
		var vmLogin = this;

		// objeto para el inicio se desion.
		vmLogin.userData = {
			username: '',
			password: ''
		};

		// Elemento para mostrar el mensaje de error
		vmLogin.error = false;

		// Evento al cambiar password
		vmLogin.changePassword = function(){
			if( vmLogin.error )
				vmLogin.error = false;
		};

		vmLogin.login = function(){
			vmLogin.error = false;
			// Mandar realizar el login de la aplicacion.
			Authentication.login(vmLogin.userData).then(function(resp){
				// EL usuario ya inicio sesion.
				$state.go('app.home');
			},function( err ){
				// No fue posible iniciar sesion
				vmLogin.userData.password = '';
				vmLogin.error = true;
			});
		};
	}
]);