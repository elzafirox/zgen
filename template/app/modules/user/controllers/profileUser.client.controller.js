'use strict';

angular.module('user').controller('profileUserController', [
	'$scope', 'Usuarios', '$stateParams', '$uibModal',
	function($scope, Usuarios, $stateParams, $uibModal) {
		// Codigo del controlador
		var vm = this;

		$scope.usuario = {};

		// Cargamos los datos del usuario
		Usuarios.get({id: $stateParams.id}).$promise.then(function(resp){
			$scope.usuario = resp.data;
		});

		// Metodo para editar los datos del usuario
		$scope.editarPerfil = function(pssw){
			pssw = pssw || false;
			$uibModal.open({
				templateUrl: 'app/modules/config/views/modalUsuario2.client.view.html',
				controller: 'modalUsuarioController',
				size: '',
				resolve: {
					usuarioData: function(){
						return angular.copy( $scope.usuario );
					},
					changePassword: function(){
						return pssw;
					}
				}
			}).result.then(function ( cn ) {
				Usuarios.get({id: $stateParams.id}).$promise.then(function(resp){
					$scope.usuario = resp.data;
				});
			});
		};

	}
]);
