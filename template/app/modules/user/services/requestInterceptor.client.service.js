'use strict';


angular.module('user').factory('RequestsInterceptor',[
	'$q', '$location', 'localStorageService',
	function($q, $location, localStorage) {
		
		var UNAUTHORIZED_CODE = 401,
			FORBIDDEN_CODE = 403,
			accessToken;

		return {
			request: function(config) {
				accessToken = localStorage.get('access_token');
				if(accessToken) config.headers.Authorization = accessToken;
				return config;
			},
			responseError: function(rejection) {
				switch(rejection.status) {
					case UNAUTHORIZED_CODE:
						if( !localStorage.get('lock') ){
							localStorage.remove('user', 'token');
							$location.path('/login');
						}
						break;
					case FORBIDDEN_CODE:
						// TODO: Add forbidden behaviour or delete
						break;
				}
				return $q.reject(rejection);
			}
		};
	}
]);