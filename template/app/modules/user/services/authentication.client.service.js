'use strict';


angular.module('user').factory('Authentication',[
		'Usuarios', '$q', 'localStorageService', '$state', '$location',
		function ( Usuarios, $q , localStorage, $state, $location) {
			return {
				user: function(){
					return localStorage.get('user') || {};
				},
				isAuth: function(){ 
					if( localStorage.get('token') ){
						return true;
					}else{
						return false;
					}
				},
				login: function(user){
					return $q(function(resolve, reject){
						// Mandamos iniciar la sesion
						Usuarios.login(user).$promise.then(function(resp){
							localStorage.set('user', resp.data.usuario);
							localStorage.set('token', resp.data.token);
							resolve(resp.data);
						}).catch(function(error){
							// Ocurrio un error la iniciar la sesion.
							reject(error);
						});
					});
				},
				logout: function(){
					localStorage.remove('user', 'token');
					$state.go('login');
				},
				lock: function(){
					if( !localStorage.get('lock') )
						localStorage.set('lock', true);
				},
				unlock: function(user){
					return $q(function(resolve, reject){
						// Mandamos iniciar la sesion
						Usuarios.unlock(user).$promise.then(function(resp){
							localStorage.remove('lock');
							resolve(resp.data);
						}).catch(function(error){
							// Ocurrio un error la iniciar la sesion.
							reject(error);
						});
					});
				}
			};
		}
	]
);
