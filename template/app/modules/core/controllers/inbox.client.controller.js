'use strict';

angular.module('core').controller('InboxController', [
	'$scope', '$state', '$interval',
	function($scope, $state, $interval) {

		var date = new Date(),
			d = date.getDate(),
			m = date.getMonth(),
			y = date.getFullYear(),
			incomingMessages = [],
			loadMessage = function(){
				$scope.messages.push(incomingMessages[$scope.scopeVariable - 1]);
				$scope.scopeVariable++;
			};

		// Avatar por default
		$scope.noAvatarImg = '/app/assets/img/default-user.png';

		// IcoMesaje
		$scope.scopeVariable = 1;

		// Mensajes
		$scope.messages = [];

	}
]);
