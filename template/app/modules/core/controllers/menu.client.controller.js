'use strict';

angular.module('core').controller('menuController', [
	'$scope', 'menuadminservice', '$rootScope', 'Menu',
	function($scope, menuadminservice, $rootScope, Menu) {

		// Codigo del controlador
		var vm = this;

		// Estructura del menu
		$scope.mdl = [];

		// Cargamos la estructura del menu
		Menu.query().$promise.then(function(resp){
			if( resp.data.length ){
				$scope.mdl = JSON.parse(resp.data[0].estructura);
			}else{
				$scope.mdl = [];
			}
		}).catch(function(e){});

		// Quedamos en la espera de que suceda el evento de cambio en menu
		$scope.$on('changed:menu', function(event, mdl){
			// Cargamos la estructura del menu
			Menu.query().$promise.then(function(resp){
				if( resp.data.length ){
					$scope.mdl = JSON.parse(resp.data[0].estructura);
				}else{
					$scope.mdl = [];
				}
			}).catch(function(e){});
		});

	}
]);
