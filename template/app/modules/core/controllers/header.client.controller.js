'use strict';

angular.module('core').controller('headerController', [
	'$scope', 'Authentication', '$location',
	function($scope, Authentication, $location) {
		// Codigo del controlador
		var vm = this;

		// Cargamos los datos del usuario
		$scope.user = Authentication.user();
		
		// Metodo para el cierre de sesion
		$scope.logout = function(){
			Authentication.logout();
		};

		// Metodo para bloquear pantalla
		$scope.lockScreen = function(){
			$location.path('/usuario/lockscreen');
			Authentication.lock();
		};

	}
]);
