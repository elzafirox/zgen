'use strict';

angular.module('core').controller('ViewMessageController', [
	'$scope', '$stateParams',
	function($scope, $stateParams) {
		var getById = function(arr, id) {
			for (var d = 0, len = arr.length; d < len; d += 1)
				if (arr[d].id === id)
					return arr[d];
		};
		$scope.message = getById($scope.messages, $stateParams.inboxID);
	}
]);
