'use strict';

/* jshint ignore:start */
angular.module('core').run([
	'$rootScope', '$state', '$stateParams', 'app', '$templateCache',
	function($rootScope, $state, $stateParams, app, $templateCache) {
		
		// Attach Fastclick for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers
		FastClick.attach(document.body);
		
		// Set some reference to access them from any scope
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;

		// GLOBAL APP SCOPE
		// set below basic information
		$rootScope.app = {
			name: app.name,
			author: app.author,
			description: app,
			version: app,
			year: ((new Date()).getFullYear()),
			isMobile: (function () {
				var check = false;
				if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
					check = true;
				};
				return check;
			})(),
			defaultLayout: {
				isNavbarFixed: app.defaultLayout.isNavbarFixed,
				isSidebarFixed: app.defaultLayout.isSidebarFixed,
				isSidebarClosed: app.defaultLayout.isSidebarClosed,
				isFooterFixed: app.defaultLayout.isFooterFixed,
				isBoxedPage: app.defaultLayout.isBoxedPage,
				theme: app.defaultLayout.theme,
				logo: app.defaultLayout.logo,
				logoCollapsed: app.defaultLayout.logoCollapsed
			},
			layout: app.layout
		};

		/**
		* ==================================================
		*	Custom UI Bootstrap Calendar Popup Template
		* ==================================================
		*/

		$templateCache.put("uib/template/datepickerPopup/popup.html",
			"<div>\n" +
			"	<ul class=\"uib-datepicker-popup clip-datepicker dropdown-menu\" dropdown-nested ng-if=\"isOpen\" ng-style=\"{top: position.top+'px', left: position.left+'px'}\" ng-keydown=\"keydown($event)\" ng-click=\"$event.stopPropagation()\">\n" +
			"		<li ng-transclude></li>\n" +
			"		<li ng-if=\"showButtonBar\" class=\"uib-button-bar\">\n" +
			"			<span class=\"btn-group pull-left\">\n" +
			"				<button type=\"button\" class=\"btn btn-sm btn-primary btn-o uib-datepicker-current\" ng-click=\"select('today', $event)\" ng-disabled=\"isDisabled('today')\">{{ getText('current') }}</button>\n" +
			"				<button type=\"button\" class=\"btn btn-sm btn-primary btn-o uib-clear\" ng-click=\"select(null, $event)\">{{ getText('clear') }}</button>\n" +
			"			</span>\n" +
			"			<button type=\"button\" class=\"btn btn-sm btn-primary pull-right uib-close\" ng-click=\"close($event)\">{{ getText('close') }}</button>\n" +
			"		</li>\n" +
			"	</ul>\n" +
			"</div>\n" +
			"");

		$templateCache.put("uib/template/datepicker/year.html",
			"<table class=\"uib-yearpicker\" role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
			"  <thead>\n" +
			"    <tr>\n" +
			"      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left uib-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
			"      <th colspan=\"{{::columns - 2}}\"><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm uib-title\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\"><strong>{{title}}</strong></button></th>\n" +
			"      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right uib-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
			"    </tr>\n" +
			"  </thead>\n" +
			"  <tbody>\n" +
			"    <tr class=\"uib-years\" ng-repeat=\"row in rows track by $index\">\n" +
			"      <td ng-repeat=\"dt in row\" class=\"uib-year text-center\" role=\"gridcell\"\n" +
			"        id=\"{{::dt.uid}}\"\n" +
			"        ng-class=\"::dt.customClass\">\n" +
			"        <button type=\"button\" class=\"btn btn-default\"\n" +
			"          uib-is-class=\"\n" +
			"            'btn-current' for selectedDt,\n" +
			"            'active' for activeDt\n" +
			"            on dt\"\n" +
			"          ng-click=\"select(dt.date)\"\n" +
			"          ng-disabled=\"::dt.disabled\"\n" +
			"          tabindex=\"-1\"><span ng-class=\"::{'text-info': dt.current}\">{{::dt.label}}</span></button>\n" +
			"      </td>\n" +
			"    </tr>\n" +
			"  </tbody>\n" +
			"</table>\n" +
			"");

		$templateCache.put("uib/template/datepicker/month.html",
			"<table class=\"uib-monthpicker\" role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
			"  <thead>\n" +
			"    <tr>\n" +
			"      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left uib-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
			"      <th><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm uib-title\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\"><strong>{{title}}</strong></button></th>\n" +
			"      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right uib-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
			"    </tr>\n" +
			"  </thead>\n" +
			"  <tbody>\n" +
			"    <tr class=\"uib-months\" ng-repeat=\"row in rows track by $index\">\n" +
			"      <td ng-repeat=\"dt in row\" class=\"uib-month text-center\" role=\"gridcell\"\n" +
			"        id=\"{{::dt.uid}}\"\n" +
			"        ng-class=\"::dt.customClass\">\n" +
			"        <button type=\"button\" class=\"btn btn-default\"\n" +
			"          uib-is-class=\"\n" +
			"            'btn-current' for selectedDt,\n" +
			"            'active' for activeDt\n" +
			"            on dt\"\n" +
			"          ng-click=\"select(dt.date)\"\n" +
			"          ng-disabled=\"::dt.disabled\"\n" +
			"          tabindex=\"-1\"><span ng-class=\"::{'text-info': dt.current}\">{{::dt.label}}</span></button>\n" +
			"      </td>\n" +
			"    </tr>\n" +
			"  </tbody>\n" +
			"</table>\n" +
			""); 


	}
]);
/* jshint ignore:end */