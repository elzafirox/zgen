'use strict';

angular.module('core').config([
	'$stateProvider', '$urlRouterProvider',
	function( $stateProvider, $urlRouterProvider ) {

		// Redirect to home view when route not found
		$urlRouterProvider.otherwise(function ($injector) {
			var $state = $injector.get('$state');
			$state.go('app.home');
		});

		// Routes
		$stateProvider
			.state('app', {
				views: {
					root: {
						controller:'CoreController',
						controllerAs:'ctCore',
						templateUrl: '/app/modules/core/views/app.client.view.html',
					}
				},
				data: {
					permissions: {
						only: ['AuthenticationUser'],
						except: 'LockUser'
					}
				},
				abstract: true
			})
			.state('app.home', {
				url:'/',
				title: 'Home',
				views: {
					'content@app': {
						controller:'HomeController',
						controllerAs:'ctHome',
						templateUrl: '/app/modules/core/views/home.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Dashboard'
				}
			});

	}
]);
