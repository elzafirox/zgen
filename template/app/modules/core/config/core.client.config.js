'use strict';

/* jshint ignore:start */
angular.module('core').config([
	'$translateProvider', 'cfpLoadingBarProvider', '$breadcrumbProvider', '$localStorageProvider', '$httpProvider', 'blockUIConfig',
	function($translateProvider, cfpLoadingBarProvider, $breadcrumbProvider, $localStorageProvider, $httpProvider, blockUIConfig) {

		/**
		* ==================================================
		*				  translate config
		* ==================================================
		*/

		// prefix and suffix information  is required to specify a pattern
		// You can simply use the static-files loader with this pattern:
		$translateProvider.useStaticFilesLoader({
			prefix: 'app/assets/i18n/',
			suffix: '.json'
		});

		// Since you've now registered more then one translation table, angular-translate has to know which one to use.
		// This is where preferredLanguage(langKey) comes in.
		$translateProvider.preferredLanguage('es');

		// Store the language in the local storage
		$translateProvider.useLocalStorage();

		// Enable sanitize
		$translateProvider.useSanitizeValueStrategy('sanitize');


		/**
		* ==================================================
		*		Angular-Loading-Bar: configuration
		* ==================================================
		*/

		cfpLoadingBarProvider.includeBar = false;
		cfpLoadingBarProvider.includeSpinner = false;

		/**
		* ==================================================
		*				BlockUi: configuration
		* ==================================================
		*/

		blockUIConfig.message = "Cargando, espere porfavor...";
		blockUIConfig.delay = 0;

		/**
		* ==================================================
		*		Angular-Loading-Bar: configuration
		* ==================================================
		*/

		$breadcrumbProvider.setOptions({
			template: '<ul class="breadcrumb"><li><a ui-sref="app.dashboard"><i class="fa fa-home margin-right-5 text-large text-dark"></i>Home</a></li><li ng-repeat="step in steps">{{step.ncyBreadcrumbLabel}}</li></ul>'
		});

		/**
		* ==================================================
		*					ng-storage
		* ==================================================
		*/

		//set a prefix to avoid overwriting any local storage variables
		$localStorageProvider.setKeyPrefix('appAngular');

		/**
		* ==================================================
		*				xeditable:config
		* ==================================================
		*/

		// editableOptions.theme = 'bs3';

		/**
		* ==================================================
		*				xeditable:config
		* ==================================================
		*/

		// calendarConfig.dateFormatter = 'moment';
		// calendarConfig.showTimesOnWeekView = false;

		/**
		* ==================================================
		*				restInterceptor:config
		* ==================================================
		*/
		$httpProvider.interceptors.push('restInterceptorLoading');
	}
]);

/* jshint ignore:end */