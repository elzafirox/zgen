'use strict';

angular.module('core').factory('restInterceptorLoading',[
	'$q', 'cfpLoadingBar', 'blockUI', 'toaster',
	function($q, cfpLoadingBar, blockUI, toaster) {
		return {
			request: function(config) {
				//cfpLoadingBar.start();
				blockUI.start();
				config.requestTimestamp = new Date().getTime();
				return config;
			},
			response: function(response) {
				//cfpLoadingBar.complete();
				blockUI.stop();
				response.config.responseTimestamp = new Date().getTime();
				return response;
			},
			responseError: function(rejection) {
				//cfpLoadingBar.complete();
				blockUI.stop();

				if(  rejection ){
					// Mostramos la notificacion del error
					if( rejection.data.hasOwnProperty('meta') ){
						toaster.pop('error', 'Ocurrio un error', rejection.data.meta.error.Description);
					}
				}
				
				// Regresamos la configuracion
				rejection.config.responseTimestamp = new Date().getTime();
				return $q.reject(rejection);
			}
		};
	}
]);

