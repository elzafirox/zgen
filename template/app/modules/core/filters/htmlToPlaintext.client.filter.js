'use strict';

//filter to convert html to plain text
angular.module('core').filter('htmlToPlaintext', function () {
		return function (text) {
			return String(text).replace(/<[^>]+>/gm, '');
		};
	}
);