'use strict';


angular.module('core').filter('importObjectFilter', function () {
		return function (item, clave) {

			if( item[clave] ){
				return item[clave];
			}else{
				if( clave.split('.').length > -1 ){
					var elementos = clave.split('.');
					return item[elementos[0]][elementos[1]];
				}
			}
		};
	}
);
