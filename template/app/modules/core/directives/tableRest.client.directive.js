'use strict';

angular.module('core').directive('tableRest', function () {
	return {
		restrict: 'E',
		scope: {
			options: '='
		},
		controller: [
			'$scope', '$injector',
			function($scope, $injector) {

				// Validamos que se pasen opciones
				if( !$scope.options )
					return false;

				// Comprovamos si se paso el source
				if( $scope.options.hasOwnProperty('source') ){
					// Comprovamos si es un string para inyectarlo
					if( angular.isString( $scope.options.source ) ){
						$scope.options.source = $injector.get( $scope.options.source );
					}
				}else{
					// No hacemos nada no tenemos un service con que trabajar.
					return 0;
				}

				// Control
				var index = null,
					met = 1,
					orderField = null,
					loadData = function(page){
						page = page || 1;

						var objectFilter = {
							filter: {
								page: parseInt(page),
								limit: parseInt($scope.paginarPor)
							}
						};

						if( $scope.order ){
							objectFilter.filter['orderby'] = $scope.order;
							objectFilter.filter['orderbytype'] = $scope.orderbytype;
						}

						// Comprovamos si esta realizando una busqueda
						if( $scope.isSearch ){
							var camp = new Object();
							camp[$scope.fieldSearch] = $scope.searchFilter
							var o = {'$eq':camp};
							objectFilter.filter['where'] = o;
						}

						$scope.options.source.query(objectFilter).$promise.then(function(resp){
							$scope.items = resp.data;
						});
					};
				
				// Encabezados
				$scope.headers = [];

				// Botones disponibles
				$scope.buttons = [];

				// Registros
				$scope.data = [];

				// Elemento que es buscado
				$scope.searchFilter = '';

				// Indicamos si estamos realizando una busqueda
				$scope.isSearch = false;

				// Indicamos si esta activo el elemento de busqueda
				$scope.seachActive = false;

				// Campo con el cual ordenar
				$scope.order = null;

				// Tipo de orden
				$scope.orderbytype = 'asc';

				// Objecto de paginacion
				$scope.paginator = {
					forPage: 10,
					totalItems: 0,
					max: 5
				};

				// Numero de paginas
				$scope.numPages = Math.ceil($scope.paginator.totalItems / $scope.paginator.forPage);

				// Numero de registros por pagina
				$scope.paginarPor = "10";

				// Elementos de la tabla
				$scope.items = [];

				// Numero de pagina actual
				$scope.currentPage = 1;

				// Validamos si se incluye el campo de busqueda
				if( $scope.options.hasOwnProperty('fieldSearch') ){
					$scope.fieldSearch = $scope.options.fieldSearch;
					$scope.seachActive = true;
				}

				// Se permite que el metodo loadData se inicialize
				$scope.loadData = function(page){
					loadData(page);
				};

				// Metodo que se ejecuta el ejecutar una busqueda
				$scope.buscar = function(){
					$scope.currentPage = 1;
					$scope.isSearch = true;
					loadData();
				};

				// Metodo para cancelar la busqueda
				$scope.clearSearch = function(){
					$scope.searchFilter = '';
					$scope.currentPage = 1;
					$scope.isSearch = false;
					loadData();
				};

				// Metodo que se ejecuta cuando cambia la pagina.
				$scope.pageChanged = function(){
					loadData($scope.currentPage);
				};

				// Metodo a ejecutar al cambiar el numero de elementos por pagina
				$scope.changedLimite = function(){
					$scope.currentPage = 1;
					$scope.paginator.forPage = parseInt( $scope.paginarPor );
					$scope.numPages = Math.ceil($scope.paginator.totalItems / $scope.paginator.forPage);
					loadData();
				};

				// Metodo que realiza el ordenamiento de los elementos
				$scope.orderby = function( $index , orderEnnabled){

					// Comprovamos si es valido
					if( !orderEnnabled )
						return 0;

					if( index !== $index ){
						if( angular.isNumber(index) ){
							$scope.headers[index].down = false;
							$scope.headers[index].up = false;
						}
						index = $index;
					}

					met = met === 1 ? 0 : 1;

					// Comprovamos el orden en el que se organizara
					if( met === 1 ){
						$scope.headers[index].down = false;
						$scope.headers[index].up = true;
						$scope.orderbytype = 'desc';
					}else{
						$scope.headers[index].down = true;
						$scope.headers[index].up = false;
						$scope.orderbytype = 'asc';
					}

					// Asignamos el campo con el que se ordenara todo
					orderField = $scope.headers[index].fiel;

					// Ejecutams la consulta
					$scope.order = orderField;
					$scope.currentPage = 1; // Regresamos a cero el elemento
					loadData();
				};

				// Comprovamos si se pasaron fields perzonalizados
				if( $scope.options.hasOwnProperty('fields') ){
					// Recorremos los campos
					for( var fiel in $scope.options.fields ){
						// Comprovamos si se paso un dato de tipo string
						if( angular.isString( $scope.options.fields[fiel] ) ){
							$scope.headers.push( { title: $scope.options.fields[fiel], fiel: fiel , up: false, down: false, order: true} );
						}else if( angular.isObject( $scope.options.fields[fiel] ) ){
							if( $scope.options.fields[fiel].hasOwnProperty('title') ){
								var order_cache = true;
								if( $scope.options.fields[fiel].hasOwnProperty('order') ){
									order_cache = $scope.options.fields[fiel].order;
								}
								$scope.headers.push( { title: $scope.options.fields[fiel].title, fiel: fiel , up: false, down: false, order: order_cache} );
							}else{
								$scope.headers.push( { title: fiel, fiel: fiel , up: false, down: false, order: true} );
							}
						}
					}
				}

				// validamos los botones
				if( !$scope.options.hasOwnProperty('actions') ){
					$scope.options.actions = false;
				}

				// Cargamos los datos basados en el soure
				if( $scope.options.hasOwnProperty('source') ){
					$scope.options.source.count().$promise.then(function(resp){
						loadData();
						$scope.paginator.totalItems = parseInt(resp.data.count);
					});
				}

				// Creamos un arreglo con todos los botones disponibles.
				if( $scope.options.hasOwnProperty('actions') ){
					for(var key in $scope.options.actions ){
						var boton = null;
						// Comprovamos si es objeto o una funcion
						if( angular.isObject( $scope.options.actions[key] ) ){

						}else{
							if( key === 'see'){
								boton = { name: key, class: 'info', icon: false };
							}else if( key === 'update'){
								boton = { name: key, class: 'success', icon: false };
							}else if( key === 'delete' ){
								boton = { name: null, class: 'danger', icon: false };
							}else{
								boton = { name: key, class: 'default', icon: false };
							}
						}
						// Agregamos el bton al registro
						if( boton )
							$scope.buttons.push();
					}
				}

				// Metodos que se ejecutan al suceder un evento en los botones
				$scope.eventClick = function(index, eventName){
					if( $scope.options.hasOwnProperty('actions') ){

						if( $scope.options.actions.hasOwnProperty(eventName) ){
							// Ejecutamos la funcion
							$scope.options.actions[eventName].call(
								$scope, 
								index, 
								angular.copy( $scope.items[index] )
							);
						}

					}
				};

			}
		],
		templateUrl: 'app/modules/core/views/partials/tableRest.client.view.html',
		link: function (scope, element, attributes, ngModel) {}
	};
});
