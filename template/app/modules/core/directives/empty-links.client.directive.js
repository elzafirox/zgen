'use strict';

/* jshint ignore:start */

/** 
  * Prevent default action on empty links.
*/
angular.module('core').directive('a', function () {
	return {
		restrict: 'E',
		link: function (scope, elem, attrs) {
			if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
				elem.on('click', function (e) {
					e.preventDefault();
				});
			}
		}
	};
});

/* jshint ignore:end */