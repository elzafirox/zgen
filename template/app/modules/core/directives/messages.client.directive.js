'use strict';

/* jshint ignore:start */
/** 
  * Returns the id of the selected e-mail. 
*/
angular.module('core').directive('messageItem', ['$location', function ($location) {
	return {
		restrict: 'EA',
		link: function (scope, elem, attrs) {
			elem.on('click tap', function (e) {
				var id = attrs.messageItem;
			});
		}
	};
}]);
/* jshint ignore:end */