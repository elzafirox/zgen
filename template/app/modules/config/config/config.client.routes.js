'use strict';

angular.module('config').config([
	'$stateProvider', '$urlRouterProvider',
	function( $stateProvider, $urlRouterProvider ) {

		// Routes
		$stateProvider
			.state('app.config', {
				title: 'Configuracion',
				url:'/config',
				ncyBreadcrumb: {
					label: 'configuracion'
				}
			})
			.state('app.config.menuadmin', {
				url:'/menu',
				title: 'Menu Admin',
				views: {
					'content@app': {
						controller:'menuAdminController',
						controllerAs:'ctMenuAdmin',
						templateUrl: '/app/modules/config/views/menuadmin.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Administrador de Menu'
				}
			})
			.state('app.config.roles', {
				url:'/roles',
				title: 'Roles de usuario',
				views: {
					'content@app': {
						controller:'rolesDeUsuarioController',
						controllerAs:'vmRoles',
						templateUrl: '/app/modules/config/views/rolesDeUsuario.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Roles de usuario'
				}
			})
			.state('app.config.usuarios', {
				url:'/usuarios',
				title: 'Usuarios',
				views: {
					'content@app': {
						controller:'usuariosController',
						controllerAs:'ctusuarios',
						templateUrl: '/app/modules/config/views/usuarios.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Usuarios registrados'
				}
			})
			.state('app.config.usuarios.permisos', {
				url:'/:id/permisos',
				title: 'Permisos',
				include: false,
				views: {
					'content@app': {
						controller:'permisosDeUsuarioController',
						controllerAs:'vmPermisos',
						templateUrl: '/app/modules/config/views/permisosDeUsuario.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Permisos de usuarios'
				}
			})
			.state('app.config.modulos', {
				url:'/modulos',
				title: 'Modulos',
				views: {
					'content@app': {
						controller:'adminisModulosController',
						controllerAs:'ctModulosAdmin',
						templateUrl: '/app/modules/config/views/adminmodulos.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Modulos del sistema'
				}
			})
			.state('app.config.estados', {
				url:'/estados',
				title: 'Formularios',
				views: {
					'content@app': {
						controller:'estadosController',
						controllerAs:'ctEstadosAdmin',
						templateUrl: '/app/modules/config/views/estados.client.view.html'
					}
				},
				ncyBreadcrumb: {
					label: 'Formularios'
				}
			})
		;

	}
]);
