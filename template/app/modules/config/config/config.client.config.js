'use strict';

angular.module('config').config([
	'localStorageServiceProvider',
	function(localStorageServiceProvider) {
		localStorageServiceProvider
			.setPrefix('app')
			.setNotify(true, true)
			.setStorageType('sessionStorage');
	}
]);
