'use strict';

angular.module('config').controller('menuAdminController', [
	'$scope', '$state', 'menuadminservice', 'uuid',
	function($scope, $state, menuadminservice, uuid) {
		var ctMenuAdmin = this,
			eliminarOpcion = function(name, arreglo){
				// Creamos el arreglo en caso de que no haya sido pasado
				arreglo = arreglo || [];
				// Recorremos buscando el objeto a eliminar
				for(var i=0; i<arreglo.length; i++ ){
					if( arreglo[i].item.state === name ){
						// Comprovamos si tiene hijos
						if( arreglo[i].hasOwnProperty('children') ){
							// Como tiene hijos pasamos a desactivarlos todos
							var _modulos_ = [];
							// optenemos el nombre de todos los estado
							for( var j = 0; j < arreglo[i].children.length; j++){
								_modulos_.push(arreglo[i].children[j].item.state);
							}
							// Recorremos todos los modulos
							for(var k = 0; k < ctMenuAdmin.modulos.length; k++){
								for(var m = 0; m < _modulos_.length; m++){
									if( ctMenuAdmin.modulos[k].name === _modulos_[m] ){
										ctMenuAdmin.modulos[k].active = false;
									}
								}
							}
						}
						// elimimanos el objeto
						arreglo.splice(i,1);
					}else{
						// Comprovamos si tiene hijos
						if( arreglo[i].hasOwnProperty('children') ){
							arreglo[i].children = eliminarOpcion(name, arreglo[i].children);
						}
					}
				}
				// Regresamos el objeto tratado
				return arreglo;
			},
			getModulosMenu = function(menuArreglo){
				var _name_modules_ = [];
				// Recorremos el arreglo con los posibles menus
				for( var i = 0; i < menuArreglo.length; i++ ){
					_name_modules_.push( menuArreglo[i].item.state );
					if( menuArreglo[i].hasOwnProperty('children') ){
						_name_modules_ = _name_modules_.concat( getModulosMenu( menuArreglo[i].children ) );
					}
				}
				return _name_modules_;
			},
			_modulos_restore_ = [],
			_mdl_restore_ = [];
		// Elementos disponibles
		ctMenuAdmin.modulos = [];
		// Estructura basica
		$scope.mdl = [];
		// Id del menu
		$scope.idMenu = '';
		// Cargamos los modulos disponibles
		menuadminservice.obtenerModulos().then(function(forms){
			ctMenuAdmin.modulos = forms;
			// optenemos la estructura actual del menu
			menuadminservice.obtenerEstructuraMenu().then(function(mdl){
				// Indicamos la estructura del menu
				$scope.mdl = mdl.estructura || {};
				// Indicamos el id del menu
				$scope.idMenu = mdl.menu || null;
				for( var k = 0; k < ctMenuAdmin.modulos.length; k++ ){
					// Pasamos a activar todos los modulos que ya se encuentren.
					var modulos_name = getModulosMenu($scope.mdl);
					// Recorremos y activamos todos los estados ya activos
					for( var i = 0; i < ctMenuAdmin.modulos[k].states.length; i++ ){
						for( var j = 0; j < modulos_name.length; j++ ){
							if( ctMenuAdmin.modulos[k].states[i].state  === modulos_name[j] ){
								ctMenuAdmin.modulos[k].states[i].active = true;
							}
						}
					}
				}
				// Guardamos las copias
				_modulos_restore_ = angular.copy(ctMenuAdmin.modulos);
				_mdl_restore_ = angular.copy($scope.mdl);
			});
		});
		// Vigilamos los cambios
		$scope.changedModule = function(item){
			// Comprovamos si es agregado o eliminado
			if( item.active ){
				$scope.mdl.push( { item: { text: item.name , state: item.state  } });
			}else{
				// Creamos una copia del arreglo menu
				var _mdl_ = angular.copy($scope.mdl);
				// Pasamos a eliminar el objeto
				$scope.mdl = [];
				// Ejecutamos la eliminacion
				$scope.mdl = eliminarOpcion(item.state, _mdl_ );
			}
		};
		// Metodo que realiza el guardado de la estrcutura del menu
		$scope.save = function(){
			menuadminservice.guardarEstrcuturaMenu( $scope.idMenu, $scope.mdl).then(function(resp){
				$scope.idMenu = resp.menu;
				// Actualizamos la copia para la restauracion
				_mdl_restore_ = [];
				_mdl_restore_ = resp.estructura;
			});
		};
		// Metodo que restaura/cancela los cambios realizados
		$scope.restore = function(){
			ctMenuAdmin.modulos = _modulos_restore_;
			$scope.mdl = _mdl_restore_;
		};
	}
]);
