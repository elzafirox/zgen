'use strict';

angular.module('config').controller('permisosDeUsuarioController', [
	'$scope', 'Modulos', 'Permisos', '$stateParams', 'uuid',
	function($scope, Modulos, Permisos, $stateParams, uuid) {
		// Codigo del controlador
		var vm = this,
			parseBoolean = function( v ){
				return v === '1';
			},
			returnId = function(stateid){
				for(var i = 0; i < $scope.usuarioPermisos.length; i++){
					if( $scope.usuarioPermisos[i].stateid === stateid )
						return $scope.usuarioPermisos[i].id;
				}
				return null;
			},
			applyPermission  = function(){
				for(var i = 0; i < $scope.usuarioPermisos.length; i++){
					$scope.permisos[ $scope.usuarioPermisos[i].stateid ] = {
						leer: $scope.usuarioPermisos[i].read,
						escribir: $scope.usuarioPermisos[i].write,
						actualizar: $scope.usuarioPermisos[i].update,
						eliminar: $scope.usuarioPermisos[i].delete
					};
				}
			};
		// Lista de modulos disponibles
		$scope.modulos = [];
		// Permisos
		$scope.permisos = {};
		// Permisos ya aplicados.
		$scope.usuarioPermisos = [];
		// Cargamos los modulos
		Modulos.query().$promise.then(function( resp ){
			// Recorremos los modulos
			resp.data.forEach(function(mod){
				// Recorremos los formularios
				mod.states.forEach(function(form){
					// Creamos el grupo de permisos
					$scope.permisos[form.id] = {
						leer: false,
						escribir: false,
						actualizar: false,
						eliminar: false
					};
				});
			});
			// Cargamos los permisos del usario.
			Permisos.query({
				'filter':{
					'where':{
						'$eq': { 'userid': $stateParams.id }
					}
				}
			}).$promise.then(function(resp){
				// Convertimos a booleanos
				for(var i = 0; i < resp.data.length; i++){
					resp.data[i].read = parseBoolean( resp.data[i].read );	
					resp.data[i].write = parseBoolean( resp.data[i].write );	
					resp.data[i].update = parseBoolean( resp.data[i].update );	
					resp.data[i].delete = parseBoolean( resp.data[i].delete );
				}
				// Guardamos los registros
				$scope.usuarioPermisos = resp.data;
				// Actualizamos la tabla de permisos
				applyPermission();
			});
			$scope.modulos = resp.data;
		}).catch(function(e){
			// Ocurrio un error al cargar los modulos
		});
		// Agregar y quitar permisos.
		$scope.permiso = function(stateid, type, value){
			var id = returnId(stateid);
			if( id ){
				var up = {};
				up[type] = value;
				// Actualizamos el registro de permisos
				Permisos.update({Id: id},up).$promise.then(function(resp){
					// Cargamos los permisos del usario.
					Permisos.query({
						'filter':{
							'where':{
								'$eq': { 'userid': $stateParams.id }
							}
						}
					}).$promise.then(function(resp){
						// Convertimos a booleanos
						for(var i = 0; i < resp.data.length; i++){
							resp.data[i].read = parseBoolean( resp.data[i].read );	
							resp.data[i].write = parseBoolean( resp.data[i].write );	
							resp.data[i].update = parseBoolean( resp.data[i].update );	
							resp.data[i].delete = parseBoolean( resp.data[i].delete );
						}
						// Guardamos los registros
						$scope.usuarioPermisos = resp.data;
						// Actualizamos la tabla de permisos
						applyPermission();
					});
				});
			}else{
				// Creamos el registro de permisos
				// uuid.v4()
				Permisos.save({
					id: uuid.v4(),
					userid: $stateParams.id,
					stateid: stateid,
					write: $scope.permisos[stateid].escribir,
					read: $scope.permisos[stateid].leer,
					delete: $scope.permisos[stateid].eliminar,
					update: $scope.permisos[stateid].actualizar
				}).$promise.then(function(resp){
					// Cargamos los permisos del usario.
					Permisos.query({
						'filter':{
							'where':{
								'$eq': { 'userid': $stateParams.id }
							}
						}
					}).$promise.then(function(resp){
						// Convertimos a booleanos
						for(var i = 0; i < resp.data.length; i++){
							resp.data[i].read = parseBoolean( resp.data[i].read );	
							resp.data[i].write = parseBoolean( resp.data[i].write );	
							resp.data[i].update = parseBoolean( resp.data[i].update );	
							resp.data[i].delete = parseBoolean( resp.data[i].delete );
						}
						// Guardamos los registros
						$scope.usuarioPermisos = resp.data;
						// Actualizamos la tabla de permisos
						applyPermission();
					});
				});
			}
		};
	}
]);
