'use strict';

angular.module('config').controller('adminisModulosController',[ 
	'$scope', '$rootScope', '$state', '$uibModal', 'Modulos',
	function($scope, $rootScope, $state, $uibModal, Modulos) {
		// Codigo del controlador
		var vm = this;

		// Variable global que contiene los modulos registrados
		var _modulos_ = appModulosApplication.map(function(e){
			return {name: e, install: true, description:'Modulo no instalado aun'};
		});

		// Elemento que contiene los modulos
		$scope.modulos = [];

		// Cargamos los modulos
		Modulos.query().$promise.then(function(resp){
			$scope.modulos = resp.data;
			// Realizamos una comparacion con los modulos para poder descartar los ya instalados
			for (var i = 0; i < $scope.modulos.length; i++) {
				for (var j = 0; j < _modulos_.length; j++) {
					if( _modulos_[j].name === $scope.modulos[i].name )
						_modulos_.splice(j,1);
				}
			}
			// Concatemos los modulos actuales del sistema.
			$scope.modulos = $scope.modulos.concat( _modulos_ );
		}).catch(function(e){
			// Ocurrio un error al consultar modulos
		});

		// Modal para editar los datos del modulo
		$scope.modulosAdd = function( index ){
			$uibModal.open({
				templateUrl: 'app/modules/config/views/modalmodulos.client.view.html',
				controller: 'modalModulosController',
				size: '',
				resolve: {
					moduloData: function(){
						return angular.copy( $scope.modulos[index] );
					}
				}
			}).result.then(function ( cn ) {
				// Comprovamos si es nuevo o editado
				// if( index > -1 ){
				// 	$scope.modulos[index] = cn;
				// }else{
				// 	$scope.modulos.push( cn );
				// }
				// Cargamos los modulos
				Modulos.query().$promise.then(function(resp){
					$scope.modulos = resp.data;
					// Realizamos una comparacion con los modulos para poder descartar los ya instalados
					for (var i = 0; i < $scope.modulos.length; i++) {
						for (var j = 0; j < _modulos_.length; j++) {
							if( _modulos_[j].name === $scope.modulos[i].name )
								_modulos_.splice(j,1);
						}
					}
					// Concatemos los modulos actuales del sistema.
					$scope.modulos = $scope.modulos.concat( _modulos_ );
				}).catch(function(e){
					// Ocurrio un error al consultar modulos
				});
			}, function () {

			});
		};
	}
]);
