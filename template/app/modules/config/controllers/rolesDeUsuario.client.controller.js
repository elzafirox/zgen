'use strict';

angular.module('config').controller('rolesDeUsuarioController', [
	'$scope', '$uibModal', 'Roles',
	function($scope, $uibModal, Roles) {
		// Codigo del controlador
		var vm = this;

		// Contenedor de roles
		$scope.roles = [];

		Roles.query().$promise.then(function(resp){
			$scope.roles = resp.data;
		}).catch(function(e){

		});

		$scope.rolAdd = function( index){
			$uibModal.open({
				templateUrl: 'app/modules/config/views/modalRol.client.view.html',
				controller: 'modalRolController',
				size: '',
				resolve: {
					rolData: function(){
						return angular.copy( $scope.roles[index] );
					}
				}
			}).result.then(function ( cn ) {
				// Comprovamos si es nuevo o editado
				if( index > -1 ){
					$scope.roles[index] = cn;
				}else{
					$scope.roles.push( cn );
				}
			}, function () {

			});
		};

	}
]);
