'use strict';

angular.module('config').controller('modalModulosController', [
	'$scope', '$uibModalInstance', 'moduloData', 'Modulos', 'uuid',
	function($scope, $uibModalInstance, moduloData, Modulos, uuid) {

		// Codigo del controlador
		var vm = this;

		$scope.modulo = {
			id: uuid.v4(),
			name: '',
			description: ''
		};

		// Comprovamos que exista el dato
		if( moduloData ){
			// Comprovamos si se paso un dato
			if( !moduloData.hasOwnProperty('install') ){
				$scope.modulo = moduloData;
			}else if ( moduloData.hasOwnProperty('install') ) {
				$scope.modulo.name = moduloData.name;
			}
		}

		// Metodo para guardar el rol
		$scope.save = function(){
			
			var met = '';

			if( !moduloData.hasOwnProperty('install') ){
				met = 'update';
			}else{
				met = 'save';
			}
			
			Modulos[met]( {id: $scope.modulo.id}, $scope.modulo).$promise.then( function(resp){
				$uibModalInstance.close(resp.data);
			}).catch( function(){
				$uibModalInstance.dismiss('cancel');
			});

		};

		// Cancelar la edicion
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}
]);
