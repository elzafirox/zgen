'use strict';

angular.module('config').controller('modalRolController', [
	'$scope', '$uibModalInstance', 'rolData', 'Roles', 'uuid',
	function($scope, $uibModalInstance, rolData, Roles, uuid) {
		// Codigo del controlador
		var vm = this;

		// Estructura del rol
		$scope.rol = {
			id: uuid.v4(),
			name: '',
			description: '',
			level: 0
		};

		// Comprovamos si vamos a editar el registro
		if( rolData ){
			rolData.level = parseInt( rolData.level );
			$scope.rol = rolData;
		}

		// Metodo para guardar el rol
		$scope.save = function(){
			
			var met = 'save';

			if(rolData){
				met = 'update';
			}
			
			Roles[met]( {id: $scope.rol.id}, $scope.rol).$promise.then( function(resp){
				$uibModalInstance.close(resp.data);
			}).catch( function(){
				$uibModalInstance.dismiss('cancel');
			});

		};

		// Cancelar la edicion
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}
]);
