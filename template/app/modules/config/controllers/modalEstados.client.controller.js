'use strict';

angular.module('config').controller('modalEstadosController', [
	'$scope', '$uibModalInstance', 'estadoData', 'Formularios', 'uuid', 'Modulos',
	function($scope, $uibModalInstance, estadoData, Formularios, uuid, Modulos) {

		// Codigo del controlador
		var vm = this;

		$scope.estado = {
			id: uuid.v4(),
			name:'',
			description: '',
			moduleid: '',
			state: ''
		};

		// Modulos disponibles
		$scope.modulos = [];

		// Cargamos los modulos disponibles
		Modulos.query().$promise.then(function(resp){
			$scope.modulos = resp.data;
		}).catch(function(e){
			// Ocurrio un error al cargar los modulos
		});

		// Comprovamos que exista el dato
		if( estadoData ){
			// Comprovamos si se paso un dato
			if( !estadoData.hasOwnProperty('install') ){
				$scope.estado = estadoData;
			}else if ( estadoData.hasOwnProperty('install') ) {
				$scope.estado.name = estadoData.name;
				$scope.estado.state = estadoData.state;
			}
		}

		// Metodo para guardar el rol
		$scope.save = function(){
			var met = '';
			if( !estadoData.hasOwnProperty('install') ){
				met = 'update';
			}else{
				met = 'save';
			}
			Formularios[met]( {id: estadoData.id}, $scope.estado).$promise.then( function(resp){
				$uibModalInstance.close(resp.data);
			}).catch( function(){
				$uibModalInstance.dismiss('cancel');
			});
		};

		// Cancelar la edicion
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
]);
