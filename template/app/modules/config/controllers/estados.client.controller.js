'use strict';

angular.module('config').controller('estadosController', [
	'$scope', 'menuadminservice', '$uibModal', 'Formularios',
	function($scope, menuadminservice, $uibModal, Formularios) {
		
		// Codigo del controlador
		var vm = this;

		// Estados Raw
		var _estados_ = menuadminservice.obtenerModulosApp();

		// Mamos los resultados
		_estados_ = _estados_.map(function(est){
			return { name: est.title, state: est.name, description: 'No instalado', install: true};
		});

		$scope.estados = [];

		// Consultamos los estados remotos
		Formularios.query().$promise.then(function(resp){
			$scope.estados = resp.data;

			// Comprovamos que no haya elementos repetidos
			for ( var i = 0; i< $scope.estados.length; i++) {
				for ( var j = 0; j< _estados_.length; j++) {
					if( _estados_[j].state === $scope.estados[i].state )
						_estados_.splice(j,1);
				}
			}

			$scope.estados = $scope.estados.concat( _estados_ );
		}).catch(function(e){
			// Ocurrio un error al consultar los estados/formularios de la aplicacion
		});

		// Modal para editar los datos del modulo
		$scope.estadoAdd = function( index ){
			$uibModal.open({
				templateUrl: 'app/modules/config/views/modalEstados.client.view.html',
				controller: 'modalEstadosController',
				size: '',
				resolve: {
					estadoData: function(){
						return angular.copy( $scope.estados[index] );
					}
				}
			}).result.then(function ( cn ) {
				// Comprovamos si es nuevo o editado
				// if( index > -1 ){
				// 	$scope.estados[index] = cn;
				// }else{
				// 	$scope.estados.push( cn );
				// }
				Formularios.query().$promise.then(function(resp){
					$scope.estados = resp.data;
					
					// Comprovamos que no haya elementos repetidos
					for ( var i = 0; i< $scope.estados.length; i++) {
						for ( var j = 0; j< _estados_.length; j++) {
							if( _estados_[j].state === $scope.estados[i].state )
								_estados_.splice(j,1);
						}
					}

					$scope.estados = $scope.estados.concat( _estados_ );
				}).catch(function(e){
					// Ocurrio un error al consultar los estados/formularios de la aplicacion
				});
			}, function () {

			});
		};

	}
]);
