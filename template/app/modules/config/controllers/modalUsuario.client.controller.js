'use strict';

angular.module('config').controller('modalUsuarioController', [
	'$scope', '$uibModalInstance', 'usuarioData', 'changePassword', 'Roles', 'Usuarios', 'uuid',
	function($scope, $uibModalInstance, usuarioData, changePassword, Roles, Usuarios, uuid) {
		// Codigo del controlador
		var vm = this;

		// Bandera para conocer si es una edicion de usuario
		$scope.editUser = false;

		// Bandera para conocer si se esta cambiando el password
		$scope.changePassword = false;

		// Elemento para la comparacion de password
		$scope.password = '';

		// Roles disponibles
		$scope.roles = [];

		// Estructura de usuario
		$scope.usuario = {
			id: uuid.v4(),
			username:'',
			rolid: '',
			email: '',
			password: ''
		};

		// Comprovamos si se esta cambiando el password
		if(changePassword){
			$scope.changePassword = true;
		}

		// Comprovamos que exista el dato
		if( usuarioData ){
			// Comprovamos si solo se cambiara el password
			if( !$scope.changePassword ){
				// Mandamos llamar los roles
				Roles.query().$promise.then(function(resp){
					$scope.roles = resp.data;
					$scope.usuario = usuarioData;
					$scope.password = $scope.usuario.password;
				}).catch(function(e){
					// Ocurrio un error al cargar los datos de los roles
				});
				$scope.editUser = true;
			}else{
				$scope.password = '';
				$scope.usuario.password = '';
			}
		}else{
			Roles.query().$promise.then(function(resp){
				$scope.roles = resp.data;
			}).catch(function(e){
				// Ocurrio un error al cargar los datos de los roles
			});
		}

		// Metodo para guardar el rol
		$scope.save = function(){
			
			var met = '';

			if(usuarioData){
				met = 'update';
				delete $scope.usuario.rol;
			}else{
				met = 'save';
			}

			if($scope.changePassword){
				delete $scope.usuario.email;
				delete $scope.usuario.rolid;
				delete $scope.usuario.username;
				delete $scope.usuario.id;
			}

			Usuarios[met]( {id: usuarioData.id}, $scope.usuario).$promise.then( function(resp){
				$uibModalInstance.close(resp.data);
			}).catch( function(){
				$uibModalInstance.dismiss('cancel');
			});

		};

		// Cancelar la edicion
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
]);
