'use strict';

angular.module('config').controller('usuariosController', [
	'$scope', '$uibModal', 'Usuarios',
	function($scope, $uibModal, Usuarios) {
		// Codigo del controlador
		var vm = this;

		// Usuarios activos actuales
		$scope.usuarios = [];

		// Mandamos solicitar todos los usuarios registrados
		Usuarios.query().$promise.then(function(resp){
			$scope.usuarios = resp.data;
		}).catch(function(e){

		});
		
		$scope.usuarioAdd = function( index, pssw ){
			pssw = pssw || false;
			$uibModal.open({
				templateUrl: 'app/modules/config/views/modalUsuario.client.view.html',
				controller: 'modalUsuarioController',
				size: '',
				resolve: {
					usuarioData: function(){
						return angular.copy( $scope.usuarios[index] );
					},
					changePassword: function(){
						return pssw;
					}
				}
			}).result.then(function ( cn ) {
				// Comprovamos si es nuevo o editado
				// if( index > -1 ){
				// 	$scope.usuarios[index] = cn;
				// }else{
				// 	$scope.usuarios.push( cn );
				// }
				Usuarios.query().$promise.then(function(resp){
					$scope.usuarios = resp.data;
				}).catch(function(e){

				});
			}, function () {

			});
		};

	}
]);
