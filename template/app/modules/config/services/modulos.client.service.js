'use strict';


angular.module('config').factory('Modulos',[
		'$resource', 'segurity',
		function ( $resource, segurity ) {
			return $resource( segurity + 'modules/:id', { id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: segurity + 'modules',
					method:'POST',
					isArray: false
				},
				'update': {
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				},
				'count':{
					url: segurity + 'modules/count',
					method:'GET',
					isArray: false
				}
			});
		}
	]
);
