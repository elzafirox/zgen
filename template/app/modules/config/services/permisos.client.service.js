'use strict';


angular.module('config').factory('Permisos',[
		'$resource', 'segurity',
		function ( $resource, segurity ) {
			return $resource( segurity + 'permissions/:Id', { Id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: segurity + 'permissions',
					method:'POST',
					isArray: false
				},
				'update': {
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				}
			});
		}
	]
);
