'use strict';


angular.module('config').factory('Usuarios',[
		'$resource', 'segurity',
		function ( $resource , segurity) {
			return $resource( segurity + 'user/:id', { id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: segurity + 'user',
					method:'POST',
					isArray: false
				},
				'update': {
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				},
				'login':{
					url: segurity + 'user/login',
					method:'POST',
					isArray: false
				},
				'unlock':{
					url: segurity + 'user/unlock',
					method:'POST',
					isArray: false
				}
			});
		}
	]
);
