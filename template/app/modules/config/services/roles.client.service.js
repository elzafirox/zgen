'use strict';


angular.module('config').factory('Roles',[
		'$resource', 'segurity',
		function ( $resource , segurity) {
			return $resource(segurity + 'roles/:id', { id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: segurity + 'roles',
					method:'POST',
					isArray: false
				},
				'update': {
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				}
			});
		}
	]
);
