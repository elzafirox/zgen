'use strict';


angular.module('config')
	.factory('menuadminservice',[
		'$state', '$q', '$localStorage', '$rootScope', 'Formularios', 'Menu', 'uuid', 'Modulos',
		function ( $state, $q, $localStorage, $rootScope, Formularios, Menu, uuid, Modulos) {

			var obtenerModulos = function(){
				return $q(function(resolve, reject) {
					Modulos.query().$promise.then(function(resp){
						resolve(resp.data);
					}).catch(function(e){
						reject(e);
					});
				});
			};

			var obtenerModulosRaw = function(){
				var modulos = $state.get();
				var newModulos =[];

				// Filtramos los estatos disponibles
				angular.forEach(modulos, function(value, key) {
					/**
					*	NOTA:
					*		Solo se omiten login, o los que no tengan una URl.
					**/
					if( value.name !== ''  && value.url &&  value.name !== 'login' ){
						if( !value.hasOwnProperty('include') ){
							var tmp = modulos[key];
							tmp.active = false;
							newModulos.push(tmp);
						}
					}
				});
				return newModulos;
			};
			
			var obtenerEstructuraMenu = function(){
				// Creamos la promesa, para poder consultar el servicio
				return $q(function(resolve, reject) {
					// Cargamos las opciones actuales del menu
					Menu.query().$promise.then(function(resp){
						if( resp.data.length ){
							resolve({
								menu: resp.data[0].id,
								estructura: JSON.parse(resp.data[0].estructura)
							});
						}else{
							reject(false);
						}
					}).catch(function(e){
						reject(e);
					});
				});
			};

			// Metodo que se encarga de guardar la estructura del menu.
			var guardarEsctructuraMenu = function( id, estructuraMenu ){
				return $q(function(resolve, reject) {
					if( id ){
						Menu.update({
							id: id
						},{
							estructura: JSON.stringify(estructuraMenu)
						}).$promise.then(function(resp){
							// Una ves se haya guardado (servicio o local) ejecutamos el evento
							$rootScope.$broadcast('changed:menu');
							resolve({
								menu: resp.data.id,
								estructura: JSON.parse(resp.data.estructura)
							});
						}).catch(function(e){
							reject(e);
						});
					}else{
						Menu.save({
							id: uuid.v4(),
							estructura: JSON.stringify(estructuraMenu)
						}).$promise.then(function(resp){
							// Una ves se haya guardado (servicio o local) ejecutamos el evento
							$rootScope.$broadcast('changed:menu');
							resolve({
								menu: resp.data.id,
								estructura: JSON.parse(resp.data.estructura)
							});
						}).catch(function(e){
							reject(e);
						});
					}
				});
			};

			return {
				obtenerModulos: obtenerModulos,
				obtenerModulosApp: obtenerModulosRaw,
				obtenerEstructuraMenu: obtenerEstructuraMenu,
				guardarEstrcuturaMenu: guardarEsctructuraMenu
			};

		}
	]
);
