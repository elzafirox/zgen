'use strict';


angular.module('config').factory('Menu',[
		'$resource', 'segurity',
		function ( $resource , segurity) {
			return $resource( segurity + 'menu/:Id', { Id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: segurity + 'menu',
					method:'POST',
					isArray: false
				},
				'update': {
					url: segurity + 'menu/:id',
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				}
			});
		}
	]
);
