'use strict';

angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular.module(ApplicationConfiguration.applicationModuleName).config([
	'$locationProvider',
	'$httpProvider',
	function($locationProvider, $breadcrumbProvider, $httpProvider) {
		$locationProvider.hashPrefix('!');
		//$httpProvider.interceptors.push('RequestsInterceptor');
	}
]);

angular.element(document).ready(function() {
	if (window.location.hash === '#_=_') window.location.hash = '#!';
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});