'use strict';


angular.module('<[%module%]>').factory('<[%name%]>Service',[
		'$resource', 'api',
		function ( $resource, api ) {
			return $resource( api + 'url/:id', { Id:'@id' },{
				'get': {
					method:'GET',
					isArray: false
				},
				'save': {
					url: api + 'url',
					method:'POST',
					isArray: false
				},
				'update': {
					method:'PUT',
					isArray: false
				},
				'query': {
					method:'GET',
					isArray: false
				},
				'remove': {
					method:'DELETE',
					isArray: false
				},
				'delete': {
					method:'DELETE',
					isArray: false
				},
				'count':{
					url: api + 'url/count',
					method:'GET',
					isArray: false
				},
				'struct':{
					url: api + 'url/struct',
					method:'GET',
					isArray: false
				}
			});
		}
	]
);
