'use strict'

var gulp = require('gulp'),
	fs = require('fs'),
	gutil = require('gulp-util'),
	runSequence = require('run-sequence');

// Recorremos la lista de tareas disponibles
fs.readdirSync('./task')
	.map( function(file){
		// Cargamos todas als tareas
		if( (/\.(js)$/i).test(file) )
			require('./task/' + file);
	});

// Agregamos la tarea por default
gulp.task('default', function () {
	
	gutil.log('Iniciando el servidor de desarrollo');

	return runSequence(
		'clean',
		'config:ng',
		'inject:developer',
		//'jshint',
		'serve'
	);

});